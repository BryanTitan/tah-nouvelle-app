<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;
?>

<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset('IE=edge') ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('twitter:title') ?>
    <?= $this->Html->meta('twitter:image') ?>
    <?= $this->Html->meta('twitter:url') ?>
    <?= $this->Html->meta('twitter:card') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('home.css') ?>
    <?= $this->Html->css('animate.css') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('icomoon.css') ?>
    <?= $this->Html->css('owl.carousel.min.css') ?>
    <?= $this->Html->css('owl.theme.default.min.css') ?>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

	<!-- Modernizr JS -->
    <?= $this->Html->script('modernizr-2.6.2.min'); ?>

</head>
<body>
	<header>
		<div class="container text-center">
			<div class="fh5co-navbar-brand">
				<a class="fh5co-logo" href="index.html">Fellow</a>
			</div>
			<nav id="fh5co-main-nav" role="navigation">
                <ul>
					<li><?= $this->Html->link( 'Index', '/pages/index', ['class' => 'active']); ?></li>
					<li><?= $this->Html->link( 'About', '/pages/about' ); ?></li>
					<li><?= $this->Html->link( 'Contact', '/pages/Contact' ); ?></li>
				</ul>
			</nav>
		</div>
	</header>
	
	<div class="owl-carousel owl-carousel1 owl-carousel-fullwidth fh5co-light-arrow animate-box" data-animate-effect="fadeIn">
		<div class="item"><a href="images/featured-1.jpg" class="image-popup"><?= $this->Html->image('featured-1.jpg', ['alt' => 'image']); ?></a></div>
		<div class="item"><a href="images/featured-2.jpg" class="image-popup"><?= $this->Html->image('featured-2.jpg', ['alt' => 'image']); ?></a></div>
	</div>

	<div id="fh5co-intro-section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2></h2> 
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel.</p>
				</div>
			</div>
		</div>
	</div><!-- end fh5co-intro-section -->

	<div id="fh5co-common-section">
		<div class="container">
			<div class="heading-section text-center">
				<h2>Qui suis-je ?</h2>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-6 col-sm-6 services-num services-num-text-right">
						<span class="number-holder">02</span>
						<div class="desc">
							<h3>Lorem ipsum dolor sit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus blandit mattis commodo. Aliquam ullamcorper diam.</p>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 services-num">
						<span class="number-holder">01</span>
						<div class="desc">
							<h3>Lorem ipsum dolor sit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus blandit mattis commodo. Aliquam ullamcorper diam.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end fh5co-common-section -->
	
	<div id="fh5co-featured-work-section">
		<div class="container-fluid">
			<div class="heading-section text-center">
				<h2>Our Projects</h2>
			</div>
			<div class="owl-carousel owl-carousel2">
				<div class="item">
					<!-- <a href="#" class="image-popup"> -->
                    <?= $this->Html->image('portfolio_pic1.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay text-center">
						<div class="desc">
							<h3>Gaming</h3>
							<span>PC</span>
						</div>
					</a>
				</div>
				<div class="item">
                    <?= $this->Html->image('portfolio_pic2.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay pop-up-overlay-color-2 text-center">
						<div class="desc">
							<h3>Basket-ball</h3>
							<span>Playground</span>
						</div>
					</a>
				</div>
				<div class="item">
                    <?= $this->Html->image('portfolio_pic3.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay pop-up-overlay-color-3 text-center">
						<div class="desc">
							<h3>DevWeb</h3>
							<span>Fullstack</span>
						</div>
					</a>
				</div>
                <div class="item">
                    <?= $this->Html->image('portfolio_pic4.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay pop-up-overlay-color-4 text-center">
						<div class="desc">
							<h3>Manga</h3>
							<span>Anime</span>
						</div>
					</a>
				</div>
				<div class="item">
					<?= $this->Html->image('portfolio_pic5.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay text-center">
						<div class="desc">
							<h3>Famille</h3>
							<span>For life</span>
						</div>
					</a>
				</div>
				<div class="item">
					<?= $this->Html->image('portfolio_pic6.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay pop-up-overlay-color-2 text-center">
						<div class="desc">
							<h3>Lisa</h3>
							<span>Juste pour toi</span>
						</div>
					</a>
				</div>
				<div class="item">
					<?= $this->Html->image('portfolio_pic7.jpg', ['alt' => 'image']); ?>
					<a href="#" class="pop-up-overlay text-center">
						<div class="desc">
							<h3>Sons</h3>
							<span>Rap US & Rap FR</span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div><!-- end fh5co-featured-work-section -->
	<div id="fh5co-blog-section">
		<div class="container">
			<div class="heading-section text-center">
				<h2>Recent Blog</h2>
			</div>
			<div class="row">
				<div class="col-md-4 blog-section">
					<span>01 <small>Décembre 2019</small></span>
					<h3><a href="#">Lorem ipsum dolor.</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac dolor eu.</p>
					<?= $this->Html->link('Voir plus.', 'Articles', ['class' =>'button']); ?>
				</div>
				<div class="col-md-4 blog-section">
					<span>02 <small>Décembre 2019</small></span>
					<h3><a href="#">Lorem ipsum dolor.</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac dolor eu.</p>
					<?= $this->Html->link('Voir plus.', 'Articles', ['class' =>'button']); ?>
				</div>
				<div class="col-md-4 blog-section">
					<span>03 <small>Décembre 2019</small></span>
					<h3><a href="#">Lorem ipsum dolor.</a></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ac dolor eu.</p>
					<?= $this->Html->link( 'Retour', '/pages/index', ['class' => 'button']); ?>
				</div>
			</div>
		</div>
	</div><!-- end: fh5co-blog-section -->

</br>
</br>

	<footer>
		<div id="footer" class="fh5co-border-line">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p>Copyright 2019<a href="#"> Bryan</a>. All Rights Reserved. <br>Made with <i class="icon-heart3 love"></i> by <a href="#">Bryan</a> / Demo Images: <a href="https://www.pexels.com/" target="_blank">Pexels</a> &amp; <a href="http://plmd.me/" target="_blank">PLMD</a> </p>
						<p class="fh5co-social-icons">
							<a href="#"><i class="icon-twitter-with-circle"></i></a>
							<a href="#"><i class="icon-facebook-with-circle"></i></a>
							<a href="#"><i class="icon-instagram-with-circle"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

<!-- jQuery -->
<?= $this->Html->script('jquery.min'); ?>
<?= $this->Html->script('jquery.easing.1.3'); ?>
<?= $this->Html->script('bootstrap.min'); ?>
<?= $this->Html->script('jquery.waypoints.min'); ?>
<?= $this->Html->script('owl.carousel.min'); ?>
<?= $this->Html->script('jquery.stellar.min'); ?>

<!-- Main JS (Do not remove) -->
<?= $this->Html->script('main'); ?>


</body>
</html>