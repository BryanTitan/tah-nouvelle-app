<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;
?>

<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset('IE=edge') ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('twitter:title') ?>
    <?= $this->Html->meta('twitter:image') ?>
    <?= $this->Html->meta('twitter:url') ?>
    <?= $this->Html->meta('twitter:card') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('home.css') ?>
    <?= $this->Html->css('animate.css') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('icomoon.css') ?>
    <?= $this->Html->css('owl.carousel.min.css') ?>
    <?= $this->Html->css('owl.theme.default.min.css') ?>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

	<!-- Modernizr JS -->
    <?= $this->Html->script('modernizr-2.6.2.min'); ?>

</head>

<body>
	<header>
		<div class="container text-center">
			<div class="fh5co-navbar-brand">
				<a class="fh5co-logo" href="index.html">Fellow</a>
			</div>
			<nav id="fh5co-main-nav" role="navigation">
                <ul>
					<li><?= $this->Html->link( 'Index', '/pages/index', ['class' => 'active']); ?></li>
					<li><?= $this->Html->link( 'About', '/pages/about' ); ?></li>
					<li><?= $this->Html->link( 'Contact', '/pages/Contact' ); ?></li>
				</ul>
			</nav>
		</div>
    </header>

	<div class="fh5co-parallax" style="background-image: url(images/hero-1.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
					<div class="fh5co-intro fh5co-table-cell">
						<h1 class="text-center">Me contacter ?</h1>
						<p>Toujours fait avec amour <i class="icon-heart3 love"></i></a></p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end: fh5co-parallax -->
	<div id="fh5co-contact-section">
		<div class="container">
			<div class="row">
				<div class="col-md-4 animate-box">
					<h3>Mon adresse</h3>
					<ul class="contact-info">
						<li><i class="icon-location-pin"></i>Quelques part à Paris</li>
						<li><i class="icon-phone2"></i>+33 1 23 45 67 89</li>
						<li><i class="icon-mail"></i><a href="#">bryan.desola@gmail.com</a></li>
						<li><i class="icon-globe2"></i><a href="#">www.bryandesola.com</a></li>
					</ul>
				</div>
				<div class="col-md-8 animate-box">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Name">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<textarea name="" class="form-control" id="" cols="30" rows="7" placeholder="Message"></textarea>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<input type="submit" value="Send Message" class="btn btn-primary">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="map" class="fh5co-map animate-box"></div>
	<!-- END map -->

    </br>
    </br>

	<footer>
		<div id="footer" class="fh5co-border-line">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p>Copyright 2019<a href="#"> Bryan</a>. All Rights Reserved. <br>Made with <i class="icon-heart3 love"></i> by <a href="#">Bryan</a> / Demo Images: <a href="https://www.pexels.com/" target="_blank">Pexels</a> &amp; <a href="http://plmd.me/" target="_blank">PLMD</a> </p>
						<p class="fh5co-social-icons">
							<a href="#"><i class="icon-twitter-with-circle"></i></a>
							<a href="#"><i class="icon-facebook-with-circle"></i></a>
							<a href="#"><i class="icon-instagram-with-circle"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

    <!-- jQuery -->
    <?= $this->Html->script('jquery.min'); ?>
    <?= $this->Html->script('jquery.easing.1.3'); ?>
    <?= $this->Html->script('bootstrap.min'); ?>
    <?= $this->Html->script('jquery.waypoints.min'); ?>
    <?= $this->Html->script('owl.carousel.min'); ?>
    <?= $this->Html->script('jquery.stellar.min'); ?>

    <!-- Main JS (Do not remove) -->
    <?= $this->Html->script('main'); ?>


</body>
</html>