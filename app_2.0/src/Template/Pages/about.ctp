<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->layout = false;
?>

<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset('IE=edge') ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('twitter:title') ?>
    <?= $this->Html->meta('twitter:image') ?>
    <?= $this->Html->meta('twitter:url') ?>
    <?= $this->Html->meta('twitter:card') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('home.css') ?>
    <?= $this->Html->css('animate.css') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('icomoon.css') ?>
    <?= $this->Html->css('owl.carousel.min.css') ?>
    <?= $this->Html->css('owl.theme.default.min.css') ?>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">

	<!-- Modernizr JS -->
    <?= $this->Html->script('modernizr-2.6.2.min'); ?>

</head>

<body>
	<header>
		<header>
		<div class="container text-center">
			<div class="fh5co-navbar-brand">
				<a class="fh5co-logo" href="index.html">Fellow</a>
			</div>
			<nav id="fh5co-main-nav" role="navigation">
                <ul>
					<li><?= $this->Html->link( 'Index', '/pages/index', ['class' => 'active']); ?></li>
					<li><?= $this->Html->link( 'About', '/pages/about' ); ?></li>
					<li><?= $this->Html->link( 'Contact', '/pages/Contact' ); ?></li>
				</ul>
			</nav>
		</div>
	</header>

	<div class="fh5co-parallax" style="background-image: url(images/hero-1.jpg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0 text-center fh5co-table">
					<div class="fh5co-intro fh5co-table-cell">
						<h1 class="text-center">About Us</h1>
						<p>Fait avec amour <i class="icon-heart3 love"></i></a></p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end: fh5co-parallax -->
	<div id="fh5co-common-section">
		<div class="container">
			<div class="heading-section text-center">
				<h2>Qui suis-je ?</h2>
			</div>
			<div class="row about">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-6 col-sm-6 services-num services-num-text-right">
						<span class="number-holder">01</span>
						<div class="desc">
							<h3>Lorem ipsum dolor sit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus blandit mattis commodo. Aliquam ullamcorper diam.</p>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 services-num">
						<span class="number-holder">02</span>
						<div class="desc">
							<h3>Lorem ipsum dolor sit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus blandit mattis commodo. Aliquam ullamcorper diam.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row about">
				<div class="col-md-10 col-md-offset-1">
					<?= $this->Html->image('about.jpg', [ 'class' => 'img-responsive', 'alt' => 'About']); ?>
				</div>
				<div class="col-md-10 col-md-offset-1">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam faucibus tristique diam mattis pharetra. Sed vel metus
					lobortis, sollicitudin justo et, elementum risus. Praesent ut mi a sem consequat elementum nec at neque. Aliquam eu eros
					eu ex tempor tincidunt congue sit amet augue. Nullam at odio vel neque pharetra venenatis. Etiam sollicitudin rhoncus
					nibh id suscipit. Morbi fermentum sodales tellus, at blandit tellus consequat a.</p>
					<blockquote>
					  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam faucibus tristique diam mattis pharetra. Sed vel metus
					lobortis, sollicitudin justo et, elementum risus. Praesent ut mi a sem consequat elementum nec at neque. Aliquam eu eros
					eu ex tempor tincidunt congue sit amet augue. Nullam at odio vel neque pharetra venenatis. Etiam sollicitudin rhoncus
					nibh id suscipit. Morbi fermentum sodales tellus, at blandit tellus consequat a.</p>
					</blockquote>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam faucibus tristique diam mattis pharetra. Sed vel metus
					lobortis, sollicitudin justo et, elementum risus. Praesent ut mi a sem consequat elementum nec at neque. Aliquam eu eros
					eu ex tempor tincidunt congue sit amet augue. Nullam at odio vel neque pharetra venenatis. Etiam sollicitudin rhoncus
					nibh id suscipit. Morbi fermentum sodales tellus, at blandit tellus consequat a.</p>
				</div>
			</div>
			<div class="heading-section text-center">
				<h2>L'équipe</h2>
			</div>
			<div class="row about">
				<div class="col-md-10 col-md-offset-1">
					<div class="col-md-6 col-sm-6">
						<div class="team-section-grid" style="background-image: url(images/author-6.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Bryan DESOLA</h3>
									<span>Pour l'instant pas grand chose ^^</span>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue.</p>
									<p class="fh5co-social-icons">
										<a href="#"><i class="icon-twitter-with-circle"></i></a>
										<a href="#"><i class="icon-facebook-with-circle"></i></a>
										<a href="#"><i class="icon-instagram-with-circle"></i></a>
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="team-section-grid" style="background-image: url(images/author-4.jpg);">
							<div class="overlay-section">
								<div class="desc">
									<h3>Bryan DESOLA</h3>
									<span>Pour l'instant pas grand chose ^^</span>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue.</p>
									<p class="fh5co-social-icons">
										<a href="#"><i class="icon-twitter-with-circle"></i></a>
										<a href="#"><i class="icon-facebook-with-circle"></i></a>
										<a href="#"><i class="icon-instagram-with-circle"></i></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- end fh5co-common-section -->

</br>
</br>

	<footer>
		<div id="footer" class="fh5co-border-line">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p>Copyright 2019<a href="#"> Bryan</a>. All Rights Reserved. <br>Made with <i class="icon-heart3 love"></i> by <a href="#">Bryan</a> / Demo Images: <a href="https://www.pexels.com/" target="_blank">Pexels</a> &amp; <a href="http://plmd.me/" target="_blank">PLMD</a> </p>
						<p class="fh5co-social-icons">
							<a href="#"><i class="icon-twitter-with-circle"></i></a>
							<a href="#"><i class="icon-facebook-with-circle"></i></a>
							<a href="#"><i class="icon-instagram-with-circle"></i></a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

<!-- jQuery -->
<?= $this->Html->script('jquery.min'); ?>
<?= $this->Html->script('jquery.easing.1.3'); ?>
<?= $this->Html->script('bootstrap.min'); ?>
<?= $this->Html->script('jquery.waypoints.min'); ?>
<?= $this->Html->script('owl.carousel.min'); ?>
<?= $this->Html->script('jquery.stellar.min'); ?>

<!-- Main JS (Do not remove) -->
<?= $this->Html->script('main'); ?>


</body>
</html>