-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 12 déc. 2019 à 12:29
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tuteuralheure_exo`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `body` text DEFAULT NULL,
  `published` tinyint(1) DEFAULT 0,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `user_key` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `title`, `slug`, `body`, `published`, `created`, `modified`) VALUES
(1, 1, 'First Post', 'first-post', 'This is the first post.', 1, '2019-12-06 16:22:20', '2019-12-06 16:22:20'),
(5, 1, 'We love that basketball', 'We-love-that-basketball', 'They\'re playing basketball\r\nWe love that basketball\r\nBasketball is my favorite sport', 0, '2019-12-09 22:36:34', '2019-12-09 22:36:34'),
(4, 1, 'Test ajout article', 'Test-ajout-article', 'C\'est cool les articles !', 0, '2019-12-09 21:27:37', '2019-12-09 21:27:37'),
(6, 1, 'Hello world', 'Hello-world', 'Lorem ipsum tu connais', 0, '2019-12-10 00:10:45', '2019-12-10 00:10:45'),
(7, 1, 'Post par id 1', 'Post-par-id-1', 'Ceci est un test', 0, '2019-12-10 00:26:01', '2019-12-10 00:26:01');

-- --------------------------------------------------------

--
-- Structure de la table `articles_tags`
--

DROP TABLE IF EXISTS `articles_tags`;
CREATE TABLE IF NOT EXISTS `articles_tags` (
  `article_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`tag_id`),
  KEY `tag_key` (`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `articles_tags`
--

INSERT INTO `articles_tags` (`article_id`, `tag_id`) VALUES
(1, 1),
(4, 4),
(5, 2),
(5, 3),
(6, 5),
(6, 6),
(7, 4);

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(191) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`id`, `title`, `created`, `modified`) VALUES
(1, 'Premier tag', '2019-12-09 23:31:15', '2019-12-09 23:31:33'),
(2, 'basket', '2019-12-09 23:31:54', '2019-12-09 23:31:54'),
(3, 'basketball', '2019-12-09 23:32:04', '2019-12-09 23:32:04'),
(4, 'test', '2019-12-09 23:32:10', '2019-12-09 23:32:10'),
(5, 'lorem ipsum', '2019-12-10 00:11:12', '2019-12-10 00:11:12'),
(6, 'devweb', '2019-12-10 00:11:20', '2019-12-10 00:11:20');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `created`, `modified`) VALUES
(1, 'cakephp@example.com', '$2y$10$unTwjpY.r5FJBWYoRgGxwuvK7ie2AT8KwGONOA5MhdTaCLpUODP8u', '2019-12-06 16:22:20', '2019-12-10 00:09:01'),
(2, 'bryan.desola@gmail.com', '$2y$10$zOanhv/lIoVaWAY.8/UXWOItfBh1CdXirvEkEWSLyHHNluVHX/EYO', '2019-12-08 19:30:00', '2019-12-10 00:08:28');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
